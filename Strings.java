import java.util.Arrays;
import java.util.Scanner;

public class Strings {
	public static void main(String [] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		char с_str_arr[] = str.toCharArray();
		String p = "object-oriented programming";
		String zam = "OOP";
		int i_arr[] = new int[str.length()];
		int i_arr_counter = 0;
		int flag = 0;
		for(int i = 0; i < str.length(); i++) {
			char tmp[] = Arrays.copyOfRange(с_str_arr, i, i + p.length());
			String substring = new String(tmp);
			if(substring.equalsIgnoreCase(p)) {
				flag++;
				if(flag == 2) {
					i_arr[i_arr_counter] = i;
					i_arr_counter++;
					flag = 0;
				}
			}
		}
		StringBuilder str_copy = new StringBuilder(str);
		for(int j = 0; j < i_arr_counter; j++) {
			str_copy.replace(i_arr[j] - j * (p.length() - zam.length()), i_arr[j] + p.length() - j * (p.length() - zam.length()), zam);
		}
		str = str_copy.toString();
		System.out.println(str);
		String new_str = "";
		char[] new_str_arr = str.toCharArray();
		for(int i = 0; i < str.length(); i++) {
			if(new_str_arr[i] == '!' || new_str_arr[i] == '.' || new_str_arr[i] == '?' || new_str_arr[i] == ',' || new_str_arr[i]  == '"' || new_str_arr[i] == '-') {
				continue;
			}else {
				new_str += new_str_arr[i];
			}
		}
		str = new_str;
		String str_arr[] = str.split(" ");
		int lat_global_counter = 0;
		int min_c_counter = str_arr[0].length();
		String min_c_str = " ";
		for(int i = 0; i < str_arr.length; i++) {
			char[] word = str_arr[i].toCharArray();
			int c_counter = 0;
			int lat_local_counter = 0;
			for(int j = 0; j < str_arr[i].length(); j++) {
				boolean word_flag = true;
				for(int l = j + 1; l < str_arr[i].length(); l++) {
					if (word[j] == word[l]) {
					word_flag = false;
					}
				}
				if(word_flag) {
					c_counter++;
				}
				if((word[j] >= 65 && word[j] <= 90) || (word[j] >= 97 && word[j] <= 122)) {
					lat_local_counter++;
				}
			}
			if(min_c_counter > c_counter) {
				min_c_counter = c_counter;
				min_c_str = str_arr[i];
				
			}
			if(lat_local_counter == str_arr[i].length()) {
				lat_global_counter++;
			}
		}
		System.out.println("Первое слово, в котором число различный символов минимально: " + min_c_str);
		System.out.println("Количество слов, содержащих только символы латинского алфавита: " + lat_global_counter);
		System.out.print("Палиндромы: ");
		for(int i = 0; i < str_arr.length; i++) {
			boolean pal_flag = true;
			char []pal_word = str_arr[i].toCharArray();
			for(int j = 0; j < pal_word.length; j++) {
				if(pal_word[j] != pal_word[pal_word.length - j - 1]) {
					pal_flag = false;
					break;
				}
			}
			if(pal_flag & str_arr[i].length() != 0) {
				System.out.print(str_arr[i] + " ");
			}
		}
	}
}